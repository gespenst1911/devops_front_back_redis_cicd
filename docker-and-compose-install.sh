#! /bin/bash

### Installe Docker
yum install yum-utils -y
if [[ $((docker -v && echo $?) | tail -n1) != 0 ]]; then
    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    yum install docker-ce docker-ce-cli containerd.io -y
fi

if ! docker info >/dev/null 2>&1; then
    systemctl start docker
fi

# Installe Docker-compose
if [[ $((docker-compose -v && echo $?) | tail -n1) != 0 ]]; then
    curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
    ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
fi