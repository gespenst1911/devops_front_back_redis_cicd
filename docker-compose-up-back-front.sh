#! /bin/bash

# Installe le projet
# echo "Down Back & Front"
# docker-compose -f /root/project/docker-compose-back-front.yml down
echo "Build image Back & Front"
docker-compose -vvv -f /root/project/docker-compose-back-front.yml build
echo "Up Back & Front"
docker-compose -vvv -f /root/project/docker-compose-back-front.yml up -d