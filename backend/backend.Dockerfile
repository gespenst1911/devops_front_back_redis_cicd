# Ali NEHME
# M1 - SI - ESGI
# 

# 
# Execution : 
#   - docker build -t gespenst/backend-esgi -f backend.Dockerfile .
#   - docker run -it  gespenst/backend-esgi bash

FROM node:slim

# On construit l'application et on la déplace dans le bon dossier pour nginx
WORKDIR /app
COPY ./backend/ ./
# COPY ../backend/ ./

EXPOSE 8081

ENV REDIS_HOST gespenst/redis-esgi:6379

# RUN npm install --production

CMD [ "node", "./server.js" ]