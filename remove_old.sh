#! /bin/bash

mkdir -vp project/
mkdir -vp /root/project/backend
mkdir -vp /root/project/frontend

rm -rf /root/project/backend/*
rm -rf /root/project/frontend/*
