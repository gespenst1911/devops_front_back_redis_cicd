#!/bin/bash

tar -czvf node_modules.tgz ./project/backend/node_modules 
sha512sum node_modules.tgz | awk '{print $1}'
rm -rf ./node_modules.tgz