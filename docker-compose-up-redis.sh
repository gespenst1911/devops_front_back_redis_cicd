#! /bin/bash

# Installe le projet
# docker-compose -f /root/project/docker-compose-redis.yml down
docker-compose -vvv -f /root/project/docker-compose-redis.yml build
docker-compose -vvv -f /root/project/docker-compose-redis.yml up -d
