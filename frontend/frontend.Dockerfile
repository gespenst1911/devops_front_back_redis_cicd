# Ali NEHME
# M1 - SI - ESGI
# 

# 
# Execution : 
#   - docker build -t gespenst/frontend-esgi -f frontend.Dockerfile .
#   - docker run -it gespenst/frontend-esgi  -p 80:80 bash

# FROM node:slim AS builder

# # On construit l'application et on la déplace dans le bon dossier pour nginx
# WORKDIR /app
# COPY ./ ./

# RUN npm install
# RUN npm run build

# RUN cp -r ./dist/* /usr/share/nginx/html
FROM nginx:stable-alpine

# port à exposer pour accéder à l'application
EXPOSE 80

# COPY ../frontend/ /usr/share/nginx/html
COPY ./frontend/dist/ /usr/share/nginx/html


# on récupère le résultat de notre conteneur de build
# COPY --from=builder /app/dist/ /usr/share/nginx/html
