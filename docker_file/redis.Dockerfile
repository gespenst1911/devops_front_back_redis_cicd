
#
# Application : 
#   - docker build -t gespenst/redis-esgi -f redis.Dockerfile .
#   ?????...........?????- docker run -it -p 6379:6379 -v data:/app/redis/volume  gespenst/redis-esgi

# 3.12
FROM redis:alpine3.10 

EXPOSE 6379

WORKDIR /app
VOLUME /data
